from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListFrom, TodoItemForm

# Create your views here.


def todo_list(request):
    todo_list_list = TodoList.objects.all()
    context = {
        "todo_list_list": todo_list_list,
    }

    return render(request, "todos/todo_list.html", context)


def todo_items(request, id):
    todo_list_detail = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list_detail": todo_list_detail,
    }

    return render(request, "todos/todo_list_detail.html", context)


def todo_list_create(request):
    if request.method == 'POST':
        todo_form = TodoListFrom(request.POST)
        if todo_form.is_valid():
            list = todo_form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        todo_form = TodoListFrom()
    context = {
        "todo_form": todo_form
    }
    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    todo = get_object_or_404(TodoList, id=id)
    if request.method == 'POST':
        todo_form = TodoListFrom(request.POST, instance=todo)
        if todo_form.is_valid():
            todo_form.save()
            return redirect("todo_list_detail", id=id)
    else:
        todo_form = TodoListFrom(instance=todo)
    context = {
        "todo_form": todo_form
    }
    return render(request, "todos/edit.html", context)

def edit_item(request, id):
    item = get_object_or_404(TodoItem, id=id)
    if request.method == 'POST':
        item_form = TodoItemForm(request.POST, instance=item)
        if item_form.is_valid():
            updated = item_form.save()
            return redirect("todo_list_detail", id=updated.list.id)
    else:
        item_form = TodoItemForm(instance=item)
    context = {
        "item_form": item_form
    }
    return render(request, "todos/update.html", context)


def delete_todo(request, id):
    todo_instance = TodoList.objects.get(id=id)
    if request.method == 'POST':
        todo_instance.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


def create_item(request):
    if request.method == 'POST':
        item_form = TodoItemForm(request.POST)
        if item_form.is_valid():
            item = item_form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        item_form = TodoItemForm()
    context = {
        "item_form": item_form
    }
    return render(request, "todos/create_item.html", context)
