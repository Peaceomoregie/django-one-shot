from django.urls import path
from todos.views import todo_list, todo_items, todo_list_create, todo_list_update, delete_todo, create_item, edit_item


urlpatterns = [
    path("", todo_list, name="todo_list_list"),
    path("<int:id>/", todo_items, name="todo_list_detail"),
    path("create/", todo_list_create, name="todo_list_create"),
    path("<int:id>/edit/", todo_list_update, name="todo_list_update"),
    path("<int:id>/delete/", delete_todo, name="todo_list_delete"),
    path("items/create/", create_item, name="todo_item_create"),
    path("items/<int:id>/edit/", edit_item, name="todo_item_update")
]
